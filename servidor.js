const express = require("express");
const servidor = express();
servidor.use(express.json());

require('./db/mongo');
const mongoose = require('mongoose');

const ModeloExemplo = mongoose.model('exemplo', {nome: String});
const objetoExemplo = new ModeloExemplo({nome: 'Apenas um exemplo'});
objetoExemplo.save().then(()=> console.log('Salvou!'));


const pessoaController = require("./controllers/pessoaController");

servidor.get('/pessoas', pessoaController.listar);
servidor.get('/pessoas/:id', pessoaController.buscarPorId);
servidor.post('/pessoas', pessoaController.salvar);
servidor.put('/pessoas/:id', pessoaController.atualizar);
servidor.delete('/pessoas/:id', pessoaController.excluir);

servidor.get('/', 
    function(req, res){
        res.send("get()");
    }
);

servidor.post('/', 
    function(req, res){
        res.send("post()");
    }
);

servidor.put('/', 
    function(req, res){
        res.send("put()");
    }
);

servidor.delete('/', 
    function(req, res){
        res.send("delete()");
    }
);

servidor.listen(3000,
    function(){
        console.log("Servidor rodando em http://localhost:3000");
    }    
);